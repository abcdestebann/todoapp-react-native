import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class Tarea extends Component {
  render() {
    return (
        <View style={styles.container}>
           <View style={styles.columnOne}>
              <Text style={styles.text}> {this.props.tarea.tarea} </Text>
            </View>
           <View style={styles.columnTwo}>
              <TouchableOpacity onPress={() => { this.props.deleteTask(this.props.tarea.key); }}>
                 <Ionicons name="md-trash" size={25} color="#f44336" />
               </TouchableOpacity>
            </View>
         </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 20,
    borderBottomColor: '#d8d5d5',
    borderBottomWidth: 1,
  },
  columnOne: {
    flex: 3,
  },
  columnTwo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  text: {
    fontSize: 20,
  },
});
