import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}> To Do App </Text>
        <TextInput
          style={styles.textInput}
          placeholder="Ingresa tu tarea"
          onChangeText={this.props.setText}
          onSubmitEditing={this.props.setTask}
          value={this.props.tarea}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#ffc107',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
    paddingHorizontal: 10,
  },
  text: {
    fontFamily: 'Verdana',
    fontSize: 20,
    fontWeight: 'bold',
    letterSpacing: 1,
  },
  textInput: {
    margin: 30,
    padding: 15,
    width: '100%',
    borderBottomColor: 'white',
    borderRadius: 5,
    backgroundColor: 'white',
  },
});

