import React, { Component } from 'react';
import { View, StyleSheet, FlatList, ActivityIndicator, Text } from 'react-native';
import Tarea from './Tarea';


export default class Body extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.loading}>
          {this.props.loading && <ActivityIndicator size="large" color="grey" />}
        </View>
        <FlatList
          data={this.props.tareas}
          renderItem={({ item }) => <Tarea tarea={item} deleteTask={this.props.deleteTask} />}
          style={styles.list}
        />
        <View style={styles.loading}>
          {this.props.tareas.length == 0 && <Text>Aún no tienes una lista de tareas.</Text>}
        </View>
      </View >
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 8,
    justifyContent: 'center',
  },
  list: {
    flex: 1,
    paddingHorizontal: 10,
  },
  loading: {
    position: 'absolute',
    alignSelf: 'center',
  },
});
