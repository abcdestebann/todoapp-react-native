import React from 'react';
import { StyleSheet, View, AsyncStorage, Button } from 'react-native';
import Header from './src/components/Header';
import Body from './src/components/Body';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      tareas: [],
      tarea: '',
      cargando: true,
    };
  }

  componentDidMount() {
    console.log('El componente ya se monto y se ve, aca se puede hacer llamados a APIs y hacer llamados del DOM');
    this.getFromMobile();
  }

  getFromMobile = () => {
    AsyncStorage.getItem('@ToDoApp:tasks')
      .then((data) => {
        this.setState({ cargando: false });
        if (data) {
          const auxTareas = JSON.parse(data);
          this.setState({ tareas: auxTareas });
        }
      })
      .catch((error) => {
        this.setState({ cargando: false });
        console.log(error);
      });
  }

  setText = (value) => {
    this.setState({
      tarea: value,
    });
  }

  addTask = () => {
    const auxTareas = [...this.state.tareas, { tarea: this.state.tarea, key: Date.now() }];
    this.saveOnMobile(auxTareas);
    this.setState({
      // Spread operator
      tareas: auxTareas,
      tarea: '',
    });
  }

  deleteTask = (id) => {
    const auxTareas = this.state.tareas.filter(tarea => tarea.key !== id);
    this.saveOnMobile(auxTareas);
    this.setState({
      tareas: auxTareas,
    });
  }

  saveOnMobile = (tareas) => {
    AsyncStorage.setItem('@ToDoApp:tasks', JSON.stringify(tareas))
      .then(data => console.log(data))
      .catch(error => console.log(error));
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          setTask={this.addTask}
          setText={this.setText}
          tarea={this.state.tarea}
        />
        <Body
          tareas={this.state.tareas}
          deleteTask={this.deleteTask}
          saveTasks={this.saveOnMobile}
          loading={this.state.cargando}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
